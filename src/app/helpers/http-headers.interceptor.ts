import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export class HttpOptionsInterceptor implements HttpInterceptor {

  constructor() { }

intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    req = req.clone({
      setHeaders: {
        'Content-Type' : 'application/json; charset=utf-8',
        'Accept'       : 'application/json',
      },
    });
    return next.handle(req);
  }

}

export const htppOptionsInterceptorProviders = [
  { provide: HTTP_INTERCEPTORS, useClass: HttpOptionsInterceptor, multi: true }
];
