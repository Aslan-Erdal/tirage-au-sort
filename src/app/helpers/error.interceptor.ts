import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HTTP_INTERCEPTORS } from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { catchError } from "rxjs/operators";

export class ErrorInterceptor implements HttpInterceptor {
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        return next.handle(req).pipe(
            catchError((response: HttpErrorResponse) => {

                let message = "Une erreur est survenue.";

                  if(response.error) {
                      if(response.status == 500) {
                          message = " est dèja dans la liste des absent.e.s!";
                          return throwError(() => message);
                      }
                  }

                return throwError(() => response.error
                );
            })
        )
    }

}

export const errorInterceptorProviders = [
  { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }
];
