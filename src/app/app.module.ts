import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { SuffixePipe } from './pipes/suffixe-pipe.pipe';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RandomPersonComponent } from './components/random-person/random-person.component';
import { HomepageComponent } from './pages/homepage/homepage.component';
import { HeaderComponent } from './partials/header/header.component';
import { FooterComponent } from './partials/footer/footer.component';
import { AbsentpageComponent } from './pages/absentpage/absentpage.component';
import { SortButtonComponent } from './components/sort-button/sort-button.component';
import { AddAbsentComponent } from './components/add-absent/add-absent.component';
import { AbsentListComponent } from './components/absent-list/absent-list.component';
import { htppOptionsInterceptorProviders } from './helpers/http-headers.interceptor';
import { errorInterceptorProviders } from './helpers/error.interceptor';
import { ArticleDefiniPipe } from './pipes/article-defini.pipe';

@NgModule({
  declarations: [
    AppComponent,
    RandomPersonComponent,
    HomepageComponent,
    SuffixePipe,
    HeaderComponent,
    FooterComponent,
    AbsentpageComponent,
    SortButtonComponent,
    AddAbsentComponent,
    AbsentListComponent,
    ArticleDefiniPipe,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [htppOptionsInterceptorProviders, errorInterceptorProviders],
  bootstrap: [AppComponent]
})
export class AppModule { }
