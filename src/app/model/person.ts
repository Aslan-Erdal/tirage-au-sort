export type Gender = "male" | "female";

export interface Person {
  id: Number,
  firstName: String,
  lastName: String,
  email: string,
  gender: Gender
}
