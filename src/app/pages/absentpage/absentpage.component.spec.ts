import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AbsentpageComponent } from './absentpage.component';

describe('AbsentpageComponent', () => {
  let component: AbsentpageComponent;
  let fixture: ComponentFixture<AbsentpageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AbsentpageComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AbsentpageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
