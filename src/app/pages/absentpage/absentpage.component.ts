import { Component, OnInit } from '@angular/core';
import { Person } from 'src/app/model/person';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-absentpage',
  templateUrl: './absentpage.component.html',
  styleUrls: ['./absentpage.component.css']
})
export class AbsentpageComponent implements OnInit {

  public absentList: Person[] = [];
  public errorMessage: String = '';

  constructor(private userService: UserService) { }

  ngOnInit(): void {
   this.getAbsents();
  }
  public getAbsents() {
    this.userService.getUserAbsentList().subscribe(datas => this.absentList = datas);
  }

  public addPersonToList(user: Person) {
    this.userService.addUserToAbsentList(user).subscribe({
      next: (v) => v,
      error: (error) => this.errorMessage = user.firstName + ' ' + user.lastName + ' ' + error
  });
    this.getAbsents();
  }


  public removeFromList(idUser: Number) {
    const existing = this.absentList.find((user: Person) => user.id === idUser);
    this.userService.deleteUserFromAbsentList(existing?.id).subscribe(() => {
      console.log("Effecé de la liste des absent.e.s");
      this.getAbsents();
    });
  }

}
