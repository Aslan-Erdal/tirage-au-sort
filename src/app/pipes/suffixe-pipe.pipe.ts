import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'suffixePipe'
})
export class SuffixePipe implements PipeTransform {

    transform(value: String, customSuffixevalue: string = "e"): string {
    return value == 'female' ? customSuffixevalue : "";
  }

}
