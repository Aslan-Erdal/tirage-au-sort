import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'articleDefini'
})
export class ArticleDefiniPipe implements PipeTransform {

  transform(value: String, customArticlevalue: string = "la"): string {
    return value == 'female' ? customArticlevalue : "le";
  }

}
