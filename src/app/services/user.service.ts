import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, Observable, throwError } from 'rxjs';
import { environment } from '../environments/environment';
import { Person } from '../model/person';

@Injectable({
  providedIn: 'root'
})
export class UserService {

 /* A variable that is used to store the apiUrl from the environment.ts file. */
  public apiUrl = environment.tirageAuSort.apiUrl;

  /**
   * The constructor function is a default function that runs when the component is loaded. The private
   * http: HttpClient variable creates an instance of the HttpClient service and assigns it to the http
   * property of the class so it can be used throughout the component
   * @param {HttpClient} http - HttpClient - This is the service that we're going to use to make the
   * HTTP requests.
   */
  constructor(private http: HttpClient) { }

 /**
  * @returns An observable of an array of Person objects.
  */
  public getUsers(): Observable<Person[]> {
    return this.http.get<Person[]>(`${this.apiUrl}/persons`);
  }

/**
 * @returns An observable of an array of Person objects.
 */
  public getUserAbsentList(): Observable<Person[]> {
    return this.http.get<Person[]>(`${this.apiUrl}/absentList`);
  }

/**
 * `getUserById` returns a `User` object
 */
  public getUserById() {}

/**
 * @param {Person} user - Person - this is the user object that we want to add to the absent list.
 * @returns An observable of type Person
 */
  public addUserToAbsentList(user: Person): Observable<Person> {
    return this.http.post<Person>(`${this.apiUrl}/absentList`, user);
    }

/**
 * This function takes an id as a parameter and returns an observable of type Person
 * @param {Number | undefined} id - Number | undefined
 * @returns A person object.
 */
  public deleteUserFromAbsentList(id: Number | undefined): Observable<Person> {
    return this.http.delete<Person>(`${this.apiUrl}/absentList/${id}`);
   }

}
