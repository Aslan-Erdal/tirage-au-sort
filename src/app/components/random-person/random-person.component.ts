import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Person } from 'src/app/model/person';
import { UserService } from 'src/app/services/user.service';
import Swal from "sweetalert2";

@Component({
  selector: 'app-random-person',
  templateUrl: './random-person.component.html',
  styleUrls: ['./random-person.component.css']
})
export class RandomPersonComponent implements OnInit {

  // all Persons array list
  public usersList: Person[] = [];
  // all absent persons array list
  public absentList: Person[] = [];
  // all present persons array list
  public allPresentPersons: Person[] = [];
  // all passed persons array list
  public allPassedPersons: Person[] = [];

  public randomedPerson!: Person | undefined;

  constructor(private userService: UserService, private router: Router) { }

  ngOnInit(): void {
    this.init();
  }

  public init() {
    this.getAllUsers();
    this.getAbsentUsers();
  }

  public getAllUsers() {
    this.userService.getUsers().subscribe(datas => {
      this.usersList = datas
      this.getPresentUsers();
    });
  }

  public getAbsentUsers() {
    this.userService.getUserAbsentList().subscribe(datas => {
      this.absentList = datas;
      this.getPresentUsers();
    });
  }

  public getPresentUsers() {
    const listOfPresentUsers = this.usersList.filter(person => {
      return this.absentList.every(absent => absent.id !== person.id)
    });
    return this.allPresentPersons = listOfPresentUsers;
  }

  public removefromPresentList(found: number) {
    return this.allPresentPersons.splice(found, 1);
  }

  public random(tab: Person[]) {
    return Math.floor(Math.random() * tab.length);;
  }

  public getAlert() {
    let timerInterval: any | number;
      Swal.fire({
        title: '🌸',
        html: 'Tout le monde est passé ! <br> La page sera réinitialisé dans 5 secondes!',
        timer: 2500,
        timerProgressBar: true,
        didOpen: () => {
          Swal.showLoading()
          const b: any = Swal.getHtmlContainer()?.querySelector('b')
          timerInterval = setInterval(() => {}, 2500)
        },
        willClose: () => {
          clearInterval(timerInterval);
          this.router.navigate(['/'])
              .then(() => {
                window.location.reload();
              });
        }
      })
  }

  public getRandomUser(user: Person[]) {

    const foundPerson = this.allPresentPersons[this.random(this.allPresentPersons)];

    const foundedIndex = this.allPresentPersons.indexOf(foundPerson);

    this.removefromPresentList(foundedIndex);

    if(!this.allPassedPersons.includes(foundPerson)) {
      this.allPassedPersons.push(foundPerson);
    }

    if(this.allPresentPersons.length === 0 && foundPerson === undefined) {
      this.getAlert();
    }

    this.randomedPerson = foundPerson;
  }

}
