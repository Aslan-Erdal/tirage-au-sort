import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Person } from 'src/app/model/person';

@Component({
  selector: 'app-sort-button',
  templateUrl: './sort-button.component.html',
  styleUrls: ['./sort-button.component.css']
})
export class SortButtonComponent {

  constructor() { }


  @Input() allPresentPersons: Person[] = []
  @Output() newRandomEvent = new EventEmitter<Person[]>();

  public randomUser() {
    console.log(this.allPresentPersons);

    this.newRandomEvent.emit(this.allPresentPersons);
  }
}
