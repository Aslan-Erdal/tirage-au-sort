import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Person } from 'src/app/model/person';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-add-absent',
  templateUrl: './add-absent.component.html',
  styleUrls: ['./add-absent.component.css']
})
export class AddAbsentComponent implements OnInit {

  public users: Person[] = [];
  public absentForm!: FormGroup; //= this.formBuilder.group({nom_personne: null});
  //public absentPerson: Person | any;

  constructor(private userService: UserService, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.getAllUsers();
    this.initAbsentList();
  }

  @Input() errorMessage!: String;
  @Output() newAbsentEvent = new EventEmitter<Person>();

  public addPersonToList() {
      if (this.absentForm.value.user !== null) {
        this.newAbsentEvent.emit(this.absentForm.value.user);
      }
  }


  public initAbsentList() {
    this.absentForm = this.formBuilder.group({
      user: [null, [Validators.required]],
    });

  }


  public getAllUsers() {
    this.userService.getUsers().subscribe(datas => this.users = datas);
  }


  onUserChange(event: any) {
    event.preventDefault();
  }

}
