import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Person } from 'src/app/model/person';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-absent-list',
  templateUrl: './absent-list.component.html',
  styleUrls: ['./absent-list.component.css']
})
export class AbsentListComponent implements OnInit {

  constructor() { }

  @Input() absentList!: Person[];
  @Output() newAbsentEvent = new EventEmitter<Number>();

  public removeFromList(value: Number) {
    this.newAbsentEvent.emit(value);
  }

  ngOnInit(): void { }

}
