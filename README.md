# Tirage au sort.

Créer un système de tirage au sort

Il faut créer un système de tirage au sort d'une personne au sein de la classe. Le système doit être inclusif et prendre en compte les absent(e)s, et ne pas rappeler une personne une seconde fois avant que tout le monde ait été appelé.

Contexte du projet

Pour permettre à tout le monde de participer en cours, une solution efficace est d'avoir un système de tirage au sort. Dans un soucis d'inclusivité, le contenu sera différent en fonction du genre de la personne tirée au sort.

Je dois pouvoir sélectionner une personne de façon aléatoire au sein des personnes de la classe. Une fois qu'une personne a été sélectionnée, je ne dois pas pouvoir la sélectionner à nouveau avant que toutes les personnes de la classe soient passées

Je dois pouvoir ajouter et retirer des absent.e.s. Les absent.e.s ne seront pas tirés au sort.

## Cloner le projet
- git clone https://gitlab.com/Aslan-Erdal/tirage-au-sort.git
- Aller dans le dossier du projet ( cd < nom du dossier > avec le terminal )
- Puis installer les extensions avec la commande 'npm install'
- finalement tapez 'ng serve' pour lancer l'application.

## Lancer le fake server ( json-server )
- Une fois que les dépendences ont été installées.
- Aller dans le dossier /src ( cd src/ en utilisant le terminal )
- lancer le serveur json en utilisant la commande ( json-server --watch db/db.json )
- le serveur est écouté sur le http://localhost:3000
- vous pouvez accéder à la liste de des personnes dans le navigateur en entrant http://localhost:3000/persons en format json.
- vous pouvez accéder à la liste de des absent.e.s dans le navigateur en entrant http://localhost:3000/absentList en format json.

